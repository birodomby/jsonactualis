package com.biro.actualis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.biro.actualis.model.Article;
import com.biro.actualis.service.ArticleService;

@Controller
public class ActualisController {
	
	@Autowired private ArticleService articleService;
	
	@GetMapping(value = "/")
	public String accueil(Model model) {
		
		model.addAttribute("article", new Article());
		List<Article> articles = articleService.findAll();
		model.addAttribute("articles", articles);
		
		return "layout/accueil";
	}
	
	     // Importer le fichier Json et clic sur Afficher
	@PostMapping(value = "/importationFichier")
	public String ImporterFichier(@ModelAttribute Article article, RedirectAttributes redirectAttributes) {
		boolean fichier = articleService.EnregistrerDataFichierImporter(article.getFile());
		   if(fichier) {
			   redirectAttributes.addFlashAttribute("success", "Fichier Importer avec Succés !");
		   }else {
			   redirectAttributes.addFlashAttribute("error", "Importation Fichier echouée !");
		   }
		return "redirect:/";
	}
	    


	
}
