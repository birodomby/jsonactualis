package com.biro.actualis.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name="article")
public class Article {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_article")
	private Long idArticle;
	@Column(name="titre_article", columnDefinition = "text")
	private String titre;
	@Column(name="corps_article", columnDefinition = "text")
	private String corps;
	@Column(name="publication_date", columnDefinition = "text")
	private String publication_date;
	@Column(name="source_article", columnDefinition = "text")
	private String source;
	@Column(name="edition", columnDefinition = "text")
	private String edition;
	@Column(name="departements", columnDefinition = "text")
	private String departements;
	@Column(name="regions", columnDefinition = "text")
	private String regions;
	@Column(name="secteurs", columnDefinition = "text")
	private String secteurs;
	@Column(name="themes", columnDefinition = "text")
	private String themes;
	@Column(name="type_fichier")
	private String typeFichier;
	
	@Transient
	private MultipartFile file;
	
	public Article() {}
	
	// generate constructeur using fiend
	public Article(String titre, String corps, String publication_date, String source, String edition,
			String departements, String regions, String secteurs, String themes, String typeFichier) {
		super();
		this.titre = titre;
		this.corps = corps;
		this.publication_date = publication_date;
		this.source = source;
		this.edition = edition;
		this.departements = departements;
		this.regions = regions;
		this.secteurs = secteurs;
		this.themes = themes;
		this.typeFichier = typeFichier;
	}
      
	   // generate getters et setters
	public Long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getCorps() {
		return corps;
	}

	public void setCorps(String corps) {
		this.corps = corps;
	}

	public String getPublication_date() {
		return publication_date;
	}

	public void setPublication_date(String publication_date) {
		this.publication_date = publication_date;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getDepartements() {
		return departements;
	}

	public void setDepartements(String departements) {
		this.departements = departements;
	}

	public String getRegions() {
		return regions;
	}

	public void setRegions(String regions) {
		this.regions = regions;
	}

	public String getSecteurs() {
		return secteurs;
	}

	public void setSecteurs(String secteurs) {
		this.secteurs = secteurs;
	}

	public String getThemes() {
		return themes;
	}

	public void setThemes(String themes) {
		this.themes = themes;
	}

	public String getTypeFichier() {
		return typeFichier;
	}

	public void setTypeFichier(String typeFichier) {
		this.typeFichier = typeFichier;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}		
    	

}
