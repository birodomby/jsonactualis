package com.biro.actualis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonActualisApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsonActualisApplication.class, args);
	}

}
