package com.biro.actualis.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.biro.actualis.model.Article;

@Repository
public interface ArticleRepository extends CrudRepository<Article, Long>{

}
