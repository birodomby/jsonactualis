package com.biro.actualis.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.biro.actualis.model.Article;

public interface ArticleService {

	List<Article> findAll();

	boolean EnregistrerDataFichierImporter(MultipartFile file);

}
