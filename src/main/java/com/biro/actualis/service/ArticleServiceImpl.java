package com.biro.actualis.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.biro.actualis.model.Article;
import com.biro.actualis.repositories.ArticleRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService{
	
	@Autowired private ArticleRepository articleRepository;

	@Override
	public List<Article> findAll() {
		return (List<Article>) articleRepository.findAll();
	}

	@Override
	public boolean EnregistrerDataFichierImporter(MultipartFile file) {
		boolean fichier = false;
		String extension = FilenameUtils.getExtension(file.getOriginalFilename());
		
		  if(extension.equalsIgnoreCase("json")) {
			  fichier = RecupereLesdonneeFichierJson(file);
		  }
		  else  if(extension.equalsIgnoreCase("csv")) {
			  fichier = RecupereLesdonneeFichierCsv(file);
		  }
		  else  if(extension.equalsIgnoreCase("xls") || extension.equalsIgnoreCase("xlsx")) {
			  fichier = RecupereLesdonneeFichierExcel(file);
		  }
		return fichier;
	}

		//  Fichier Json
	private boolean RecupereLesdonneeFichierJson(MultipartFile file) {
		
		try {
			
			InputStream inputStream = file.getInputStream();
			ObjectMapper mapper = new ObjectMapper();
			List<Article> articles = Arrays.asList(mapper.readValue(inputStream, Article[].class));
			
			if(articles!=null && articles.size()>0) {
				
				for(Article article : articles) {
					article.setTypeFichier(FilenameUtils.getExtension(file.getOriginalFilename()));
					articleRepository.save(article);
				}		
			}
		 return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	        // Fichier CSV
	private boolean RecupereLesdonneeFichierCsv(MultipartFile file) {
		
      try {
			InputStreamReader reader = new InputStreamReader(file.getInputStream());
			CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
			List<String[]> lignes = csvReader.readAll();
			  for(String[] row : lignes) {
				  articleRepository.save(new Article(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8],
						  FilenameUtils.getExtension(file.getOriginalFilename())));
			  }
			
		 return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	         
	           // Fichier Excel
	   private boolean RecupereLesdonneeFichierExcel(MultipartFile file) {
			Workbook workbook = getWorkBook(file);
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> lignes = sheet.iterator();
			lignes.next();
			   while(lignes.hasNext()) {
				  Row row = lignes.next();
				  Article article = new Article();
				    if(row.getCell(0).getCellType() == Cell.CELL_TYPE_STRING) {
				    	article.setTitre(row.getCell(0).getStringCellValue());
				    }
				    if(row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
				    	article.setCorps(row.getCell(1).getStringCellValue());
				    }
				    if(row.getCell(2).getCellType() == Cell.CELL_TYPE_STRING) {
				    	article.setPublication_date(row.getCell(2).getStringCellValue());
				    }
				    if(row.getCell(3).getCellType() == Cell.CELL_TYPE_STRING) {
				    	article.setSource(row.getCell(3).getStringCellValue());
				    }
				    if(row.getCell(4).getCellType() == Cell.CELL_TYPE_STRING) {
				    	article.setEdition(row.getCell(4).getStringCellValue());
				    }
				    if(row.getCell(5).getCellType() == Cell.CELL_TYPE_STRING) {
				    	article.setDepartements(row.getCell(5).getStringCellValue());
				    }
				    if(row.getCell(6).getCellType() == Cell.CELL_TYPE_STRING) {
				    	article.setRegions(row.getCell(6).getStringCellValue());
				    }
				    if(row.getCell(7).getCellType() == Cell.CELL_TYPE_STRING) {
				    	article.setSecteurs(row.getCell(7).getStringCellValue());
				    }
				    if(row.getCell(8).getCellType() == Cell.CELL_TYPE_STRING) {
				    	article.setThemes(row.getCell(8).getStringCellValue());
				    }
				    
				    article.setTypeFichier(FilenameUtils.getExtension(file.getOriginalFilename()));
				       // insertion
				    articleRepository.save(article);
			   }
			   
			return true;
		}
	                 // es extension du fichiers excels
	   private Workbook getWorkBook(MultipartFile file) {
		   Workbook workbook = null;
		   String extension = FilenameUtils.getExtension(file.getOriginalFilename());
			
		   try {
			   if(extension.equalsIgnoreCase("xlsx")) {
				   workbook = new XSSFWorkbook(file.getInputStream());
			   }else if(extension.equalsIgnoreCase("xls")) {
				   workbook = new HSSFWorkbook(file.getInputStream());
			   }
			} catch (Exception e) {
				e.printStackTrace();
			}
		   return workbook;
	   }

	
	

}
